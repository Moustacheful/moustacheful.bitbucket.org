'use strict';

angular.module('initModule',[]).run(function($rootScope,mq){
	//Set up break points, you can access these anywhere in angular on template level or scope.$watch
	$rootScope.matchMedia = mq;
	mq.add('desktop','(min-width:1025px)');
	mq.add('tablet','(min-width:361px) and (max-width: 1024px)');
  mq.add('mobile','(max-width:360px)');
	mq.add('at2x','all and (-webkit-min-device-pixel-ratio: 1.5)');

})
// Declare app level module which depends on filters, and services
angular.module('productSelector', [
  'productSelector.filters',
  'productSelector.services',
  'productSelector.directives',
  'productSelector.controllers',
  'ngAnimate',
  'ngTouch',
  'matchMedia',
  'initModule'
]);

