'use strict';

/* Directives */

angular.module('productSelector.directives', []).
directive('bgSrc', function() {
	/*
	*	Allows setting the background image of an element via HTML attributes
	*	@attributes bg-src	translates to background-image css
	*	@attributes bg-width	width for background-size
	*	@attributes bg-height	height for background-size
	*/
	return function(scope, el, attrs) {
		el.css('backgroundImage', 'url(' + attrs.bgSrc + ')')
		if(attrs.bgWidth && attrs.bgHeight) {
			el.css('backgroundSize',attrs.bgWidth + '  ' + attrs.bgHeight );
		}
	};
}).
directive('iscroll', function($timeout, $rootScope) {
	/*
	*   Enables and takes care of iScroll instances on target element with default options
	*   iScroll instance is automatically destroyed along with scope (used when switching
	*   responsive states)
	*/
	var configs = {
		default:{
			scrollX: true,
			scrollY: false,
			momentum: true,
			scrollbars: 'custom',
			eventPassthrough: true,
			//mouseWheel: true,
			interactiveScrollbars: true
		},
		snap:{
			scrollX: true,
			scrollY: false,
			momentum: true,
			eventPassthrough: true,
			snap: 'img',
			mouseWheel: true
		}
	}
	return function(scope, el, attrs) {
		var scroll;
		var config = attrs.iscroll? attrs.iscroll : 'default';

		$timeout(function() {
			scroll = new IScroll(el[0], configs[config] );

			switch(config){
				case 'snap':
				
					var onResize = function(){
						el.find('img').width($(window).width())
					};

					$(window).on('resize',onResize);
					
					scope.$on('$destroy',function(){
						$(window).off('resize',onResize);
					});

					scope.$on('iscrollRefresh', function() {
						$timeout(function() {
							if(!scroll) return;
							$(window).resize();
							scroll.refresh();
						})
					});

					scope.$watch('carousel.current',function(index){
						scroll.goToPage(index,0,250);
					});

					scroll.on('scrollEnd',function(){
						scope.carousel.current = scroll.currentPage.pageX;
						scope.$digest();
					});

				break;

				default:
					scope.$on('iscrollRefresh', function() {
						$timeout(function() {
							if(!scroll) return;
							scroll.refresh();
							scroll.scrollTo(0, 0);
							// Hide scrollbar if element is not scrollable (canvas is larger than scrollable element)
							if (el.find('.iScrollIndicator').css('display') == "none") {
								el.addClass('iscroll-noscroll')
							} else {
								el.removeClass('iscroll-noscroll');
							};
						});
					})
				break;
			}
		}, 100);

		scope.$on('$destroy', function() {
			//	Destroy iScroll instance when it's not needed 
			scroll.destroy();
			scroll = null;
		})
	}
}).directive('htmlContent', function() {
	/*
	*   Puts static HTML into element. Does NOT compile (no template tags or custom 
	*   directives), if required, see http://docs.angularjs.org/api/ng.$compile 
	*/
	return function (scope, element, attrs) {
		element.html(scope.$eval(attrs.htmlContent));
	};
}).directive('section',function($timeout){
	return function (scope,element,attrs) {
		scope.$watch('status.progress',function(progress){
			if(progress != scope.index) return
			$timeout(function(){
				$.scrollTo(element,250);
			},500);
		})
	};
});
