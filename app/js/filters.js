'use strict';

/* Filters */

angular.module('productSelector.filters', []).
filter('dollars', function() {
    return function(amount) {
        return Number(amount).toFixed(2);
    }
}).
filter('nocents',function(){
	return function(amountString){
		return amountString.split('.')[0];
	}
}).
filter('astext',function(){
	return function(number){
		return {
			1:'One',
			2:'Two',
			3:'Three',
			4:'Four',
			5:'Five',
			6:'Six',
			7:'Seven',
			8:'Eight',
			9:'Nine'
		}[number];
	}
})