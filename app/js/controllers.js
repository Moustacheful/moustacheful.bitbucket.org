'use strict';

/* Controllers */

angular.module('productSelector.controllers', []).
controller('MainCtrl', function($scope,Products,$filter,$location) {
	//Main application states
	
	//debugging state
	//$scope.debug = true;

	//application status
	$scope.status = {
		quoteReady: false,
		matchingQuote: false,
		progress: 0
	};

	//quote
	$scope.quote = {
		selectedProduct: null,
		selectedPlan: null,
		deviceQuantity: null,
		onlinePayments: null
	};

	//watch the quote for results
	$scope.$watch('quote', function(updated,previous) {
		if (
			updated.selectedProduct &&
			updated.deviceQuantity &&
			updated.onlinePayments
		) {
			if(updated.deviceQuantity.fee==null){
				$scope.status.matchingQuote = false;
			}else{

				if(updated.selectedPlan) {
					//plan is selected
					if(updated.selectedPlan.fee == null){
						$scope.status.matchingQuote = false;
					}else{
						$scope.status.matchingQuote = true;
					}
				}else{
					// No plan

					$scope.status.matchingQuote = false;
					$scope.status.quoteReady = false;
					return;
				}
			}

			$scope.status.quoteReady = true;
		} else {
			$scope.status.quoteReady = false;
		}

	}, true);



	//	Preselected product (when coming from other page, based on product IDs)
	//	e.g.: url/#?select=1 where 1 is the id of the product to be selected
	$scope.preselectId = $location.search().select;
	
	//store the products on the main controller
	Products.fetch().success(function(data){
		$scope.products = data.products;
	});


	$scope.updateProgress = function(index){
		index+=1;
		if($scope.status.progress < index){
			$scope.status.progress = index;
			$scope.$broadcast('scrollToIndex',index);
		}
	}
}).

controller('ProductSelectorCtrl', function($scope,$rootScope) {
	$scope.index = 0;

	$scope.$watch('products.length',function(){
		
		//	preselection process with preselectId
		if(!$scope.preselectId) return;
		angular.forEach($scope.products,function(product){
			if($scope.preselectId == product.id){
				$scope.selectProduct(product)
			}
		});
	})
	//Handle product selection
	$scope.selectProduct = function(product) {
		if (product == $scope.quote.selectedProduct) return;
		$scope.quote.selectedProduct = product;
		$scope.quote.selectedPlan = null
		$scope.updateProgress($scope.index);
	};

	$scope.showZoom = function(index){
		if(isNaN(index)) index = 0;
		$rootScope.$broadcast('showZoom',true,index);
	};

	
}).

controller('MobileSelectorCtrl',function($scope){
	$scope.carousel = {
		current: $scope.preselectId?$scope.preselectId:0
	}
	
	$scope.move = function(newIndex) {
		if(newIndex<0) newIndex = $scope.products.length-1;
		$scope.carousel.current = newIndex%$scope.products.length;
	}

	// CurrentId product index
	$scope.$watch('products',function(products){
		if(!products) return;
		$scope.activeProduct = [ $scope.products[$scope.carousel.current] ];
		$scope.$broadcast('iscrollRefresh');

	})
	$scope.$watch('carousel.current',function(index){
		if(index==undefined) return;
		if(!$scope.products) return;
		$scope.activeProduct = [ $scope.products[$scope.carousel.current] ];
	})

}).

controller('DeviceQuantityCtrl', function($scope,Products) {
	$scope.index = 1;
	Products.fetch().success(function(data){
		$scope.options = data.quantity;
	});

	$scope.selectDeviceQuantity = function(option) {
		$scope.quote.deviceQuantity = option;
		if(option.fee == null){
			$scope.quote.selectedPlan = null;
			$scope.updateProgress($scope.index+1);
		}else{
			$scope.updateProgress($scope.index);
		}
	}
}).

//Handle plan selection, based on selectedProduct
controller('PlanSelectorCtrl',function($scope){
	$scope.index = 2;
	$scope.$watch('quote.selectedProduct.plans',function(newPlans){
		if(!newPlans) return;
		$scope.$emit('iscrollRefresh');
	},true);
	$scope.selectPlan = function(plan) {
		$scope.quote.selectedPlan = plan;
		$scope.updateProgress($scope.index);
	}
}).

controller('OnlinePaymentsCtrl',function($scope){
	$scope.index= 3;
	$scope.$watch('quote.onlinePayments',function(val){
		if(val==undefined) return;
		$scope.updateProgress($scope.index);
	})
}).

controller('SummaryCtrl', function($scope) {
	$scope.index = 4;
	$scope.gotoApplicationForm = function(){
		// Do something here, TBD.
		console.log($scope.quote)
	}
})
