'use strict';

/* Services */

angular.module('productSelector.services', []).
factory('Products',function($http,$rootScope){
    var svc = {
        fetch: function(force){
            //if(force === true && svc.cache)
            var url = typeof jsonURL !== 'undefined' ? jsonURL: 'mock-devices.json';
            return $http.get(url,{cache:true});
        }
    };
      
    return svc;
});
angular.module('matchMedia', []).
factory('mq',function($rootScope){
    var mqls = {}
    var svc = {

        add: function(id,matchMediaStr){
            mqls[id] = window.matchMedia(matchMediaStr);
            mqls[id].listener = function(m){
                svc[id] = m.matches;
                $rootScope.$apply();
            }; 
            mqls[id].addListener(mqls[id].listener)
            svc[id] = mqls[id].matches;
        },
        remove: function(id){
            mqls[id].removeListener(mqls[id].listener);
            delete mqls[id];
            delete svc[id];
            return true;

        }
    };

    return svc;
})