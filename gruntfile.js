module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		less: {
			development: {
				paths: 'app/css/',
				files: {
					"app/css/styles.css":"app/css/app.less"
				}
			}
		},
		watch: {
			styles: {
				files: 'app/css/*.less',
				tasks: ['less'],
				/*options: {
					livereload: true
				}*/
			}

		}
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-less');
	// Default task(s).
	grunt.registerTask('default', ['watch']);

};
