Commbank SME product selector
=============================

Temporary repository for the SME (_small & medium enterprises_) product selector.


Dependencies
------------

- **AngularJS** framework v1.2.7 ([docs](http://code.angularjs.org/1.2.7/docs/)) and
 - _angular-animate.js_ for animations
 - _angular-touch.js_ for handling of swipes
- **iScroll** library for handling smooth-scrolling panels v5.0.5 ([github](github.com/cubiq/iscroll))
- **jQuery** _sadly_ required for selectors v1.10.2 (for supporting older browsers - [docs](http://api.jquery.com/))
- **Transit** jQuery plugin for CSS3 transitions and extension to jquery's .css method v0.9.9 ([docs](http://ricostacruz.com/jquery.transit/?utm_source=html5weekly)) - _could be dropped, for now just used for extra helper cross-browser $(*).css properties - see directive **product-zoom**_


Notes
-----
**JSON structure**: The data structure for the JSON service is defined in _'mockup-devices.json'_. Please keep it up to date if it changes.

- **product.keyFeatures** property is meant to be static HTML code coming from a rich text editor. It won't be compiled (meaning no custom directives or template tags. If such functionality is required, see [here](http://docs.angularjs.org/api/ng.$compile).

**Responsive features** this application uses a matchMedia [service](https://bitbucket.org/Moustacheful/angular-matchmedia/overview) which provides angular with direct access to media queries, allowing to have different templates depending on the viewport state. Media queries are configured in app.js and the viewport state is located in $rootScope.matchMedia. mediaMatch. The [matchMedia.js](https://github.com/paulirish/matchMedia.js/) polyfill may be installed for older browser support.